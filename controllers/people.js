const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const { getAllPeople } = require('../services/StarWarsAPIService');

// @desc      Get people
// @route     GET /api/v1/people
// @access    Public
exports.getPeople = asyncHandler(async (req, res, next) => {
    const SORT_BY_OPTIONS = ['name', 'height', 'mass'];

    const { sortBy } = req.query;

    if (
        sortBy !== 'undefined' &&
        sortBy !== undefined &&
        sortBy !== null &&
        !SORT_BY_OPTIONS.includes(sortBy)
    ) {
        return next(
            new ErrorResponse(
                `The sortBy parameter of '${sortBy}' is invalid.`,
                400
            )
        );
    }

    const commonResponse = await getAllPeople(sortBy);

    res.status(200).json({
        success: commonResponse.isError,
        data: commonResponse.data,
    });
});
