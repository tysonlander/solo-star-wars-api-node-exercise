const asyncHandler = require('../middleware/async');
const {
    getAllPlanets,
    setPlanetResidentsFullNames,
} = require('../services/StarWarsAPIService');
const { default: axios } = require('axios');

// @desc      Get Planets
// @route     GET /api/v1/planets
// @access    Public
exports.getPlanets = asyncHandler(async (req, res, next) => {
    const planetsCommonResponse = await getAllPlanets();

    if (planetsCommonResponse.isError) {
        return res.status(200).json({
            success: planetsCommonResponse.isError,
            data: planetsCommonResponse.data,
        });
    }

    const namesCommonResponse = await setPlanetResidentsFullNames(planetsCommonResponse.data);

    res.status(200).json({
        success: namesCommonResponse.isError,
        data: namesCommonResponse.data,
    });
});
