require('dotenv').config();
const path = require('path');
const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const colors = require('colors');
const cookieParser = require('cookie-parser');
const helmet = require('helmet');
const xss = require('xss-clean');
const rateLimit = require('express-rate-limit');
const hpp = require('hpp');
const cors = require('cors');
const errorHandler = require('./middleware/error');

// Route
const peopleRouter = require('./routes/people');
const planetsRouter = require('./routes/planets');

const app = express();

// Body parser
app.use(express.json());

// Cookie parser
app.use(cookieParser());

// Dev logging middleware
if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
}

// Set security headers
app.use(helmet());

// Prevent XSS attacks
app.use(xss());

// Rate limiting
const limiter = rateLimit({
    windowMs: 10 * 60 * 1000, // 10 mins
    max: 100,
});
app.use(limiter);

// Prevent http param pollution
app.use(hpp());

// Enable CORS
app.use(cors());

// // Mount routers
app.use('/api/v1/people', peopleRouter);
app.use('/api/v1/planets', planetsRouter);

app.use(errorHandler);

const PORT = process.env.PORT || 8080;
const server = app.listen(
    PORT,
    console.log(
        // @ts-ignore
        `Server running in ${process.env.NODE_ENV} mode on port ${PORT}`.yellow
            .bold
    )
);

// Handle unhandled promise rejections
process.on('unhandledRejection', (err, promise) => {
    // @ts-ignore
    console.log(`Error unhandledRejection: ${err.message}`.red);
});

// Handle uncaught exception
process.on('uncaughtException', (err) => {
    // @ts-ignore
    console.log(`Error uncaughtException: ${err.message}`.red);
});
