const express = require('express');
const {getPeople} = require('../controllers/people')

const router = express.Router();

router.route('/').get(getPeople)

module.exports = router;