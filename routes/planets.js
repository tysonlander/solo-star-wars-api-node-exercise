const express = require('express');
const { getPlanets } = require('../controllers/planets');

const router = express.Router();

router.route('/').get(getPlanets);

module.exports = router;
