const getFloatFromString = (string) => {
    let num = 0;
    try {
        if (string === null || string === undefined || string === 'undefined') {
            return 0;
        }

        num = parseFloat(string.replace(/[^0-9.]/g, ''));
        if (isNaN(num)) {
            return 0;
        }
    } catch (error) {
        return 0;
    }
    return num;
};

module.exports = {getFloatFromString}