const { default: axios } = require('axios');
const {getFloatFromString} =require('./NumberService')

const SWAPI_BASE_URL = 'https://swapi.dev/api';

const getAllPlanets = async () => {
    const commonResponse = {
        isError: false,
        message: '',
        data: [],
    };

    try {
        let allPlanets = [];
        let resp;
        let nextUrl = `${SWAPI_BASE_URL}/planets`;

        while (nextUrl !== undefined && nextUrl !== null) {
            resp = await axios.get(nextUrl);
            if (resp.status === 200) {
                allPlanets.push(...resp.data.results);
                nextUrl = resp.data.next;
            } else {
                commonResponse.error = true;
                commonResponse.message = 'Error fetching data';
                return commonResponse;
            }
        }
        commonResponse.data = allPlanets;
    } catch (error) {
        commonResponse.error = true;
        commonResponse.message = 'Error fetching data';
    }

    return commonResponse;
};

const setPlanetResidentsFullNames = async (planets) => {
    const commonResponse = {
        isError: false,
        message: '',
        data: [],
    };
    if (planets.length < 1) return commonResponse;

    try {
        let allPeopleResp = await getAllPeople(null);
        if(allPeopleResp.isError){
            commonResponse.isError
            commonResponse.message = allPeopleResp.message
            return commonResponse
        }
        let allPeople = allPeopleResp.data

        let fullNameList;
        for (let planetIndex = 0; planetIndex < planets.length; planetIndex++) {
            // for list of panet residents on the planet get a list of full names
            fullNameList = [];
            for (
                let residentIndex = 0;
                residentIndex < planets[planetIndex].residents.length;
                residentIndex++
            ) {
                fullNameList.push(findNameByPersonUrl(planets[planetIndex].residents[residentIndex], allPeople))
            }
            planets[planetIndex].residents = fullNameList;
        }

        commonResponse.data = planets;
    } catch (error) {
        commonResponse.error = true;
        commonResponse.message = 'Error fetching planet resident data.';
    }

    return commonResponse;
};

const findNameByPersonUrl = (personUrl, people) => {
    let name = ''
    for(let i = 0; i < people.length; i++){
        if(personUrl === people[i].url)
            return people[i].name
    }
    return name;
}

const getAllPeople = async (sortBy) => {
    const commonResponse = {
        isError: false,
        message: '',
        data: [],
    };

    try {
        let allPeople = [];
        let resp;
        let nextUrl = `${SWAPI_BASE_URL}/people`;

        while (
            nextUrl !== undefined &&
            nextUrl !== 'undefined' &&
            nextUrl !== null
        ) {
            resp = await axios.get(nextUrl);
            if (resp.status === 200) {
                allPeople.push(...resp.data.results);
                nextUrl = resp.data.next;
            } else {
                commonResponse.error = true;
                commonResponse.message = 'Error fetching data';
                return commonResponse;
            }
        }

        commonResponse.data = await sortPeople(allPeople, sortBy);
    } catch (error) {
        commonResponse.error = true;
        commonResponse.message = 'Error fetching data';
    }

    return commonResponse;
};

const sortPeople = async (people, sortBy) => {
    const SORT_BY_OPTIONS = ['name', 'height', 'mass'];
    if (!SORT_BY_OPTIONS.includes(sortBy)) {
        return people;
    }

    people = people.sort((a, b) => {
        let fa = a[sortBy],
            fb = b[sortBy];

        if (sortBy === 'name') {
            // nulls sorted before anything else
            if (fa === null || fa === undefined || fa === 'undefined') {
                return -1;
            }
            if (fb === null || fb === undefined || fb === 'undefined') {
                return 1;
            }

            fa.toLowerCase();
            fb.toLowerCase();
        } else {
            fa = getFloatFromString(fa);
            fb = getFloatFromString(fb);
        }

        if (fa < fb) return -1;
        if (fa > fb) return 1;
        return 0;
    });
    return people;
};

module.exports = { getAllPlanets, setPlanetResidentsFullNames, getAllPeople };
